- 👋 Hi, I’m Ahmed Hassine
- 👀 I’m interested in Full Stack developping and DevOps
- 🌱 I’m currently learning Software Architect at EQL
- 💞️ I’m looking to collaborate on ambitious projects on Cloud Computing
- 📫 How to reach me : By phone : +33 7 82 27 44 87 or By mail : m.ahmed.hassine@gmail.com or LinkedIn profile : https://www.linkedin.com/in/m-ahmed-hassine/

- you can take a look to my personal website https://www.m-ahmed-hassine.fr
